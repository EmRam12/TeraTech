json.partial! "tickets/ticket", ticket: @ticket
json.id @ticket.id
json.images_url url_for(@ticket.images)