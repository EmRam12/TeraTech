json.extract! product, :id, :titulo, :precio, :descripcion, :created_at, :updated_at
json.url product_url(product, format: :json)
