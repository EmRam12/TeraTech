// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//= require rails-ujs
//= require trix
//= require bootstrap-sprockets
//= require turbolinks
//= require moment
//= require bootstrap-datetimepicker
//= require pickers

require("@fortawesome/fontawesome-free")


import "jquery"
import 'bootstrap'
require("turbolinks").start()
require("packs/custom")

import Rails from "@rails/ujs"
Rails.start()
import * as ActiveStorage from "@rails/activestorage"
ActiveStorage.start()
import "channels"




//bootstrap
import 'bootstrap/dist/js/bootstrap'
import 'bootstrap/dist/css/bootstrap'
require("stylesheets/application.scss")

//fontawesome
import "@fortawesome/fontawesome-free/css/all"

document.addEventListener("turbolinks:load", function() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
        $('[data-toggle="popover"]').popover()
    })
  })
