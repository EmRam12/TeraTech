class Product < ApplicationRecord
    validates :titulo, presence: true
    validates :descripcion, presence: true
    validates :precio, presence: true

    has_many_attached :images
end
