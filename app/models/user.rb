class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  after_create :assign_default_role
  accepts_nested_attributes_for :roles,
    allow_destroy: true,
    reject_if: ->(hash){ hash["_keep"] != "1" }
  has_many :tickets
  
  def assign_default_role
    if User.count == 1
      self.add_role(:admin) if self.roles.blank?
      self.add_role(:tecnico)
    else
      self.add_role(:usuario)
    end
  end

  def self.create_new_user(params)
    @user = User.create!(params)
  end

  def to_s
    email
  end  

end
