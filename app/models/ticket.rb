class Ticket < ApplicationRecord
    extend SimpleCalendar
    validates :title,   :presence => true
                    
    validates :description, :presence => true
    validates :estados, presence: true
    validate :overlapping_appointments
    validates :telefono,    :numericality => true,
                            on: :create,
                            :length => { :minimum => 10, :maximum => 15, :message => "El Telefono debe tener 10 caracteres" }
    validate :check_dates

    belongs_to :user
    has_many :users
    has_one_attached :images
    
    def check_dates
        if self.starts_at.present?
            a = self.starts_at
            errors.add(:starts_at, "La cita no puede ser en el pasado") if( a < Time.now)
        end
    end

    def initialize(params={})
        super(params)
    end

    def start_time
        self.starts_at
    end

    def overlapping_appointments
        if (Ticket.where("(? BETWEEN starts_at AND ends_at OR ? BETWEEN starts_at AND ends_at) AND user_id = ?", self.starts_at, self.ends_at, self.user_id).any?)
           errors.add(:ends_at, 'Se empalma con otra reserva')
        end
    end

    def to_s
        title
    end 
end
