class Tecnico < ApplicationRecord
    belongs_to :user
    belongs_to :ticket
    has_many :tickets
end
