class TicketsController < ApplicationController
  before_action :set_ticket, only: %i[ show edit update destroy ]
  # GET /tickets or /tickets.json
  def index
    if current_user.has_any_role? :admin
      @tickets = Ticket.all
    else
      @tickets = Ticket.where(:user_id => current_user.id)
    end
  end

  # GET /tickets/1 or /tickets/1.json
  def show
    tickets = Ticket.find(params[:id])
  end

  # GET /tickets/new
  def new
    @tickets = Ticket.all
    @ticket = Ticket.new
  end

  def set_n
    tickets = Ticket.find(params[:id])
    if tickets.params[:remoto] = true
      tickets.params[:starts_at] = null
      tickets.params[:ends_at] = null
    end
  end
  # GET /tickets/1/edit
  def edit
    @users = User.all
  end

  # POST /tickets or /tickets.json
  def create
    @users = User.all
    @user = current_user
    @tickets = Ticket.all
    @ticket = Ticket.new(ticket_params)
    @ticket.user = current_user
    @starts_at = params[:starts_at]

      if @ticket.save 
        redirect_to @ticket, notice: "Se creo el ticket correctamente"
      else
        render :new, status: :unprocessable_entity
      end
  end

  def addtecnico
    ticket = Ticket.find(params[:id])
    @ticket.users << User.find(params[:user_id])
    respond_to do |format|
      format.html { redirect_to @ticket, :notice => 'Added.' }
    end
  end

  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to @ticket, notice: "Ticket was successfully updated." }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1 or /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: "Ticket was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def count_status
    @ticket = Ticket.where(estados: 'Pendiente')
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def ticket_params
      params.require(:ticket).permit(:title, :description, :estados, :remoto, :direccion, :starts_at, :ends_at, :telefono, :images, images: [], users: [] ) 
    end
end
