class UsersController < ApplicationController
    before_action :set_user, only: [:edit, :update]

    def index
        @users = User.all.order(created_at: :desc)
        @q = User.ransack(params[:q])
        @users = @q.result(distinct: true)
    end

    def edit
        @user = User.find(params[:id])
    end

    def update
        @user = User.find(params[:id])
        if @user.update(user_params)
          redirect_to users_path, notice: 'El usuario ha sido actualizado'
        else
          render 'edit'
        end
    end

    def new
        @user = User.new(user_params)
        if @user.save
            redirect_to users_path, notice: "User succesfully created!" 
        else
            render :new
        end
    end
    
    def create
        @user = User.new(params[:user])
        if @user.save
          redirect_to users_path
        else
          render :new
        end
    end

    def show
        sign_out :user
        redirect_to static_pages_landing_page_path
    end

    def destroy
        @user = User.find(params[:id])
        @user.destroy
    
        if @user.destroy
            redirect_to root_url, notice: "User deleted."
        end
    end

    private

    def set_user
        @user = User.find(params[:id])
    end 

    def user_params
        params.permit(:email, :name, :last_name, :roles, :password, :password_confirmation)
    end

    def user_params
        params.require(:user).permit({role_ids: []})
    end

    def update_params
        user_params.tap do |o|
          # Adds the _delete attribute if the keep checkbox is unchecked
          o[:roles_attributes] = o[:roles_attributes].map do |k,h|
             attrs = h.merge(_destroy: (h[:_keep] != "1"))
             # Don't let the user update the name of an existing Role!
             # This would let a malicious user to grant any role.
             h.key?(:id) ? attrs.except(:name) : attrs 
          end
        end
    end
end
