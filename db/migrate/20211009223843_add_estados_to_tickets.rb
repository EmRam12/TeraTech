class AddEstadosToTickets < ActiveRecord::Migration[6.1]
  def change
    add_column :tickets, :estados, :string, default: 'Pendiente'
  end
end
