class AddDiaToTickets < ActiveRecord::Migration[6.1]
  def change
    add_column :tickets, :dia, :date
  end
end
