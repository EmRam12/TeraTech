class AddHoraToTickets < ActiveRecord::Migration[6.1]
  def change
    add_column :tickets, :hora, :time
  end
end
