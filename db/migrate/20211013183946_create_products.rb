class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :titulo
      t.numeric :precio
      t.string :descripcion

      t.timestamps
    end
  end
end
