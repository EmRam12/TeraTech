class AddDireccionToTickets < ActiveRecord::Migration[6.1]
  def change
    add_column :tickets, :direccion, :string
  end
end
