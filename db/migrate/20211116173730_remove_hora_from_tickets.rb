class RemoveHoraFromTickets < ActiveRecord::Migration[6.1]
  def change
    remove_column :tickets, :hora, :time
  end
end
