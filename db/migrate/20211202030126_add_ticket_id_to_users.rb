class AddTicketIdToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :ticket_id, :integer
  end
end
