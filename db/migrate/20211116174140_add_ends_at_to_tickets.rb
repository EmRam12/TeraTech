class AddEndsAtToTickets < ActiveRecord::Migration[6.1]
  def change
    add_column :tickets, :ends_at, :datetime
  end
end
