class AddTelefonoToTickets < ActiveRecord::Migration[6.1]
  def change
    add_column :tickets, :telefono, :numeric
  end
end
