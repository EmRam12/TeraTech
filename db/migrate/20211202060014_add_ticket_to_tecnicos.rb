class AddTicketToTecnicos < ActiveRecord::Migration[6.1]
  def change
    add_reference :tecnicos, :ticket, null: false, foreign_key: true
  end
end
