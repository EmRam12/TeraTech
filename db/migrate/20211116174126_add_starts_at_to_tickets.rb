class AddStartsAtToTickets < ActiveRecord::Migration[6.1]
  def change
    add_column :tickets, :starts_at, :datetime
  end
end
