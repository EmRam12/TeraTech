class RemoveEstadoFromTickets < ActiveRecord::Migration[6.1]
  def change
    remove_column :tickets, :estado, :string
  end
end
