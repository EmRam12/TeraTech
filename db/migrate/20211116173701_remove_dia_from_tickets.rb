class RemoveDiaFromTickets < ActiveRecord::Migration[6.1]
  def change
    remove_column :tickets, :dia, :date
  end
end
