class AddUserToTecnicos < ActiveRecord::Migration[6.1]
  def change
    add_reference :tecnicos, :user, null: false, foreign_key: true
  end
end
