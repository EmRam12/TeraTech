Rails.application.routes.draw do
  resources :products
  
  resources :tickets do 
    collection do
      get :addtecnico
    end
  end
  
  get 'static_pages/landing_page'
  root to: "static_pages#landing_page"
  
  devise_for :users
    match 'users/:id' => 'users#destroy', :via => :delete, :as => :admin_destroy_user
  resources :users

  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
